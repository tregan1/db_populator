import numpy as np
import pandas as pd
import logging
import datetime
from pathlib import Path
import os
from tqdm import tqdm
import random
import time

import generator_utils as gu


def retrieve_header_info(engine, hwu_ids):

    if len(hwu_ids) == 1:
        hwu_ids = f"({hwu_ids[0]})"
    else:
        hwu_ids = tuple(hwu_ids)
    
    stmt = f"""select 
Channels.[id] as "ChId",
Channels.[type] as "ChType",
Channels.[channelNum] + 1 as "nStart_channel",
convert(varchar(max), convert(varbinary(4),HardwareUnits.[serialNumber]),2) as "HW_SN",
APSets.[resolution],
APSets.[maxFreq],
case
    when APSets.[resolution] = 400 then 1024
	when APSets.[resolution] = 800 then 2048
	when APSets.[resolution] = 1600 then 4096
	when APSets.[resolution] = 3200 then 8192
	when APSets.[resolution] = 6400 then 16384
	when APSets.[resolution] = 12800 then 32768
	when APSets.[resolution] = 25600 then 65536
	when APSets.[resolution] = 51200 then 131072
	else NULL
end as "WavePoints",
case
    when APSets.[maxFreq] = 61 then 156.25
	when APSets.[maxFreq] = 122 then 312.5
	when APSets.[maxFreq] = 244 then 625
	when APSets.[maxFreq] = 488 then 1250
	when APSets.[maxFreq] = 977 then 2500
	when APSets.[maxFreq] = 1953 then 5000
	when APSets.[maxFreq] = 3906 then 10000
	when APSets.[maxFreq] = 7813 then 20000
	else NULL
end as "SampleRate"
from Channels
left join HardwareUnits on Channels.hardwareUnitId = HardwareUnits.id
left join APSets on Channels.apsetId = APSets.id
where Channels.type = 0 and Channels.hardwareUnitId in {hwu_ids}"""

    results = gu.sql_query(stmt, engine)
    #cast(APSets.[resolution] as varchar)

    return results


def generate_timestamps(num_waves):
    timestamps = []
    init_dt = datetime.datetime.now() - datetime.timedelta(minutes=15)

    for i in range(num_waves):
        timestamps.append(init_dt - datetime.timedelta(minutes=i*240))

    return timestamps


def construct_header(row, year, month, day, hour, minute, second):

    header = [f";PodID {row['HW_SN'].lower()}",
f";Date Year({year}) Month({month}) Day({day}) Hour({hour}) Minutes({minute}) Seconds({second})",
f";FSampleRate {row['SampleRate']}",
";Channels 1",
f";nStart_channel {row['nStart_channel']}",
";Units 0",
";Agc 1",
f";Samples {int(row['WavePoints'])}",
";RMS 0.069516",
f";channelIds {row['ChId']} -1"]

    return header


def read_sample_files(combinations):

    # ref_files: dict = {}

    # path = 'sample_waves\\'

    # for file in os.listdir('sample_waves\\'):

    #     with open(path+file) as f:
    #         lines = f.readlines()
    #         lines = [line.rstrip() for line in lines]
        
    #     ref_files[f"{file}"] = lines

    ref_files: dict = {}

    path = 'sample_waves\\'
    
    for combination in combinations:
        rate, samples = combination.split('_')
        file = (f"R{rate}__N{samples}.txt")

        with open(path+file) as f:
            lines = f.readlines()
            lines = [line.rstrip() for line in lines]

        ref_files[f"{file}"] = lines

    return ref_files


def generate_ac_files(ac_header_table, num_waves, timestamps, ts1_path):
    """
    Generates AC files to be consumed by the Python API.
    """

    print("Generating AC sample files....")

    #Only read in sample files that are needed based on the apset combinations
    ac_header_table['Combo'] = ac_header_table.apply( lambda row: str(int(row['SampleRate'])) + '_' + str(int(row['WavePoints'])), axis=1)
    combinations = ac_header_table['Combo'].unique()

    ref_files = read_sample_files(combinations)

    for _, row in tqdm(ac_header_table.iterrows(), total=ac_header_table.shape[0]):

        directory_name = (f"{row['HW_SN'].lower()}_ch{row['nStart_channel']}")

        dir_path = Path(f"{ts1_path}\\{directory_name}")

        if not dir_path.exists():
            dir_path.mkdir(exist_ok=True)
        
        ref_file_lines = ref_files[f"R{int(row['SampleRate'])}__N{int(row['WavePoints'])}.txt"]
        
        # Creates ts1 file for each timestamp in the list of generated timestamps
        for record in timestamps:
            year = str(record.year)
            month = (f"{record.month:02d}")
            day = (f"{record.day:02d}")
            hour = (f"{record.hour:02d}")
            minute = (f"{record.minute:02d}")
            second = (f"{record.second:02d}")

            file_name = Path(f"{year}_{month}_{day}__{hour}_{minute}_{second}.txt")

            header = construct_header(row, year, month, day, hour, minute, second)

            full_path = os.path.join(dir_path, file_name)

            samples = row['WavePoints']

            changer = 0.2 * np.random.random_sample((samples,))

            mod_data = np.array(ref_file_lines, dtype=np.float32) * changer
            #mod_data = [float(val) * random.uniform(0.02,0.15) for val in ref_file_lines]

            with open(full_path, "w") as f:
                for line in header:
                    f.write(line + "\n")
                
                np.savetxt(f, mod_data, fmt='%s', delimiter='\n')

    return []


def retrieve_dc_info(engine, hwu_ids):

    if len(hwu_ids) == 1:
        hwu_ids = f"({hwu_ids[0]})"
    else:
        hwu_ids = tuple(hwu_ids)

    stmt_batt = f"""select 
                    convert(varchar(max),convert(varbinary(4), HardwareUnits.serialNumber),2) as "HW_SN",
                    Channels.id as "BatteryChannel"
                    from Channels
                    left join HardwareUnits on Channels.hardwareUnitId = HardwareUnits.id
                    where Channels.channelNum = 1 and channels.type = 1 and Channels.hardwareUnitId in {hwu_ids}"""

    battery_results = gu.sql_query(stmt_batt, engine)

    stmt_temp = f"""select 
                    convert(varchar(max),convert(varbinary(4), HardwareUnits.serialNumber),2) as "HW_SN",
                    Channels.id as "TempChannel"
                    from Channels
                    left join HardwareUnits on Channels.hardwareUnitId = HardwareUnits.id
                    where Channels.channelNum = 0 and channels.type = 1 and Channels.hardwareUnitId in {hwu_ids}"""

    temp_results = gu.sql_query(stmt_temp, engine)

    dc_results = pd.merge(battery_results, temp_results, how='left', left_on='HW_SN', right_on='HW_SN')

    return dc_results


def generate_dc_files(dc_header_table, num_waves, timestamps, ts1_path):

    print("Generating DC files...")
    for _, row in tqdm(dc_header_table.iterrows(), total=dc_header_table.shape[0]):

        dir_path = Path(f"{ts1_path}\\dcvals")

        if not dir_path.exists():
            dir_path.mkdir(exist_ok=True)
        
        for record in timestamps:
            year = str(record.year)
            month = (f"{record.month:02d}")
            day = (f"{record.day:02d}")
            hour = (f"{record.hour:02d}")
            minute = (f"{record.minute:02d}")
            second = (f"{record.second:02d}")

            file_name = Path(f"DC_{row['HW_SN'].lower()}_{year}_{month}_{day}__{hour}_{minute}_{second}.txt")

            data = (f"{year}_{month}_{day}__{hour}_{minute}_{second} {row['HW_SN'].lower()} 0003 {round(random.uniform(0.5, 1.2),5)}" +\
                f" {round(random.uniform(1.0, 2.7),5)} -1.00000 -1.00000 -1.00000 -1.00000 -1.00000 -1.00000 -1.00000 -1.00000 -1.00000 " +\
                f"-1.00000 -1.00000 -1.00000 -1.00000 -1.00000 {row['TempChannel']} {row['BatteryChannel']} -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 0 ;")

            full_path = os.path.join(dir_path, file_name)

            with open(full_path, "w") as f:
                f.write(data)

    return []


def generate_files(engine, hwu_ids, schematic, data):
    #def generate_files(engine, channel_ids, hwu_ids, schematic, data):

    timestamps = generate_timestamps(schematic["num_waves"])

    #AC
    ac_header_table = retrieve_header_info(engine, hwu_ids)
    generate_ac_files(ac_header_table, schematic["num_waves"], timestamps, data["ts1_file_path"])

    #DC
    dc_header_table = retrieve_dc_info(engine, hwu_ids)
    generate_dc_files(dc_header_table, schematic["num_waves"], timestamps, data["ts1_file_path"])

    return
