from sqlalchemy.types import NVARCHAR
import numpy as np
import pandas as pd
import logging

import generator_utils as gu


def generate_tags(engine, hwu_ids):

    if len(hwu_ids) == 1:
        hwu_ids = f"({hwu_ids[0]})"
    else:
        hwu_ids = tuple(hwu_ids)

    stmt = f"""
        SELECT [Channels].[id] as "channelId"
	    ,[WaveParams].[id] as "waveParamId"
	    , 1 as "enabled"
	    ,case 
	    when [Channels].[type] = 0 then [Areas].[name] + '.' + [Assets].[name] + '.' + [Channels].[name] + '.' + [WaveParams].[name]
	    else [Areas].[name] + '.' + [Assets].[name] + '.' + [Channels].[name] + '.' + [APSets].[name]
	    end as "tag"
	    , 1 as "worldviewSystemId"
        FROM {gu.wv_db}.[dbo].[Channels]
        left join [HardwareUnits] on [Channels].[hardwareUnitId] = [HardwareUnits].[id]
        left join [WaveParams] on [Channels].[apsetId] = [WaveParams].[apsetId]
        left join [APSets] on [Channels].[apsetId] = [APSets].[id]
        left join [Assets] on [Channels].[assetId] = [Assets].[id]
        left join [Areas] on [Assets].[areaId] = [Areas].[id]
        where [HardwareUnits].[id] in {hwu_ids}"""

    tags = gu.sql_query(stmt, engine)

    return tags


def insert_export_tags(engine, hwu_ids):
    tags = generate_tags(engine, hwu_ids)

    tags = tags.where(pd.notnull(tags), None)

    logging.info(f"Inserting export tags into {gu.wv_db} ... ")

    connection = engine.raw_connection()
    insert_template = f"Insert into ExportTags (channelId, waveParamId, enabled, tag, worldviewSystemId) values (?,?,?,?,?)"
    cursor = connection.cursor()
    cursor.fast_executemany = True
    cursor.executemany(insert_template, tags.values.tolist())
    cursor.commit()
    cursor.close()
    connection.close()

    logging.info(f"Successfully inserted {len(tags)} export tags into {gu.wv_db}.")

    return []
