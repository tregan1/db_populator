import logging
import json
import pandas as pd
import pyodbc
from sqlalchemy import create_engine
import math

import generator_utils as gu
import structure as struct
import waveform_gen as wg
import live_exports as le
import maintenance_priorities as mp
import no_files as nf


def determine_waves_per_channel(schematic):
    """
    Function to determine the number of waves to generate per channel 
    for a target database size. Minimum number of waves per channel is one.
    """

    WAVES_PER_GB = 8000 # This ratio is not exact and can be changed

    waves_per_channel = math.ceil((schematic["data_size"]*(WAVES_PER_GB)) / (schematic["num_mists"] * 2))

    return waves_per_channel
    

def db_design():
    """
    Generates the blueprint for what data will be created and added to the database.
    """
    default_setup = ''
    okay = ''
    schematic = {}

    while not (default_setup.lower() == 'y' or default_setup.lower() == 'n'):
        default_setup = input("Proceed with default max load database simulation (22 Areas, 1500 Assets, 25000 mist units)? y / n ")

        if default_setup.lower() == 'y':
            print("Continuing with 22 areas, 1500 assets, 25000 mistlx units.")
            schematic["num_areas"] = 22
            schematic["num_assets"] = 1500
            schematic["num_mists"] = 25000
        elif default_setup.lower() == 'n':
            schematic["num_areas"] = int(input("How many areas in this setup? "))
            schematic["num_assets"] = int(input("How many assets in this setup? "))
            schematic["num_mists"] = int(input("How many mist hardware units in this setup? "))
        
        schematic["wave_data"] = input("Would you like to generate data for this configuration? [y/n] ")

        if schematic["wave_data"].lower() == 'y':
            while okay.lower() != 'y':
                schematic["data_size"] = float(input("Appoximately how many GB of data would you like to generate? "))
                schematic["num_waves"] = determine_waves_per_channel(schematic)
                okay = input(f"This will generate approximately {schematic['num_waves']} waves per AC channel, is this okay? [y/n] ")

    return schematic


def read_configs():
    """
    Reads in the configs.json and sets default values for the setup.
    """
    try:
        with open('configs.json', 'r') as file:
            data: dict = json.load(file)
            logging.info("Loading settings from config file...")
        return data
    except Exception as e:
        logging.error(f"No config file found, cannot proceed without sql connection information. {e}")


def db_connect(data: dict):

    try:
        logging.info(f"Connecting to {gu.wv_db} database...")
        engine_path = (f"mssql+pyodbc://worldview:Uptime100@{data['worldview_db_ip']}:1433/{data['worldview_db']}?driver=ODBC+Driver+{data['worldview_odbc_driver']}+for+SQL+Server")
        engine = create_engine(engine_path)
        # test connection and exist program if fails
        with engine.begin() as connection:
            pd.read_sql_query("SELECT DB_NAME()", connection)
            gu.wv_db = engine.url.database
            logging.info("Database connection successful!")
        return engine
    except Exception as e:
        logging.error(f"Error trying to establish connection to the worldview database. Cannot continue. {e}")


def start_db_generation():
    '''
    Initial function call to start the check of the field app for any new information to insert into worldview.
    '''

    logging.basicConfig(filename='generator.log', filemode='a+', format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)

    logging.info("-----------------------------------------------------------------------------------------")
    logging.info("-------------------------------  STARTING DATA GENERATION  ------------------------------")
    logging.info("-----------------------------------------------------------------------------------------")

    # Initialize any global variables
    gu.initialize()

    schematic = db_design()

    data = read_configs()

    engine = db_connect(data)
    
    new_hwu_ids = struct.create_structure(engine, schematic, data["hwu_to_base_ratio"], data["default_base_ip"], data["alset_per_channel"])

    if schematic["wave_data"] == 'y':
        if data["no_files"] == False:
            wg.generate_files(engine, new_hwu_ids, schematic, data)
        else:
            nf.insert_ac_data(engine, new_hwu_ids, schematic["num_waves"])

    if data["live_exports"] == True:
        le.insert_export_tags(engine, new_hwu_ids)
    
    if data["maintenance_priorities"] == True:
        # Infer number of channels based on #hardwareUnits * 4
        mp_channels = math.ceil(len(new_hwu_ids)*4*data["priority_channel_percent"]/100.0)

        mp.generate_maintenance_priorities(engine, mp_channels)

    logging.info("")
    logging.info("-------------------------------  DATA GENERATION COMPLETE  ------------------------------")
    logging.info("")

    return True


if __name__ == "__main__":
    success = start_db_generation()
    if success:
        print("Done.")
    else:
        print("Error occurred while attempting to populate database. Consult logs for more info.")
    close_input = input()
