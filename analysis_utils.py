from sqlalchemy.orm import sessionmaker
import pandas as pd
import logging
from tqdm import tqdm
import numpy as np

import generator_utils as gu

def _insert_apsets(engine, APSets, WaveParams, ALSets, AlarmLimits):

    apset_ids: list = []

    results = pd.DataFrame(columns=["SensorType", "APSet_Id", "ALSet_Id"])

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        print(f"Inserting apsets into the database...")

        for ix, row in APSets.iterrows():
            insert = []
            aps_stmt = (f"""
                Insert into APSets (name, sensorType, process, minFreq, maxFreq, resolution, average, frequencyUnit,  frequencyPlotUnit,
                waveformUnit, spectralUnit, spectralAmpFactor, autoscaleWaveform, autoscaleSpectral, orderEnabled, isDemod, logEnabled,
                waveformLogEnabled, worldviewSystemId) output {ix}, inserted.[id], inserted.[sensorType] values('{row["name"]}', {row["sensorType"]}, 
                {row["process"]}, {row["minFreq"]}, {row["maxFreq"]}, {row["resolution"]}, {row["average"]}, {row["frequencyUnit"]}, 
                {row["frequencyPlotUnit"]}, {row["waveformUnit"]}, {row["spectralUnit"]}, {row["spectralAmpFactor"]},{row["autoscaleWaveform"]}, 
                {row["autoscaleSpectral"]}, {row["orderEnabled"]}, {row["isDemod"]}, {row["logEnabled"]}, {row["waveformLogEnabled"]}, 
                {row["worldviewSystemId"]})
                """)

            cursor.execute(aps_stmt)
            result = cursor.fetchall()[0]
            
            for i in range(len(result)):
                insert.append(result[i])

            apset_ids.append(insert)

            _, APS, SensorType = result

            # Replace any NaN values with NULL
            WaveParams.fillna('NULL', axis = 1, inplace=True)
            # Wave params to insert on this iteration match the intended apset
            wp_insert = WaveParams[WaveParams["APS_REF_ID"] == (ix+1)]

            wp_ids: list = []

            for _, param in wp_insert.iterrows():
                insert_wp = []

                wp_stmt = (f"""
                    Insert into WaveParams (paramNum, name, type, freqUnitsType, paramUnitsType, paramAmpFactorType, minFreq, maxFreq, alarmOnLocation, 
                    apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values({param["paramNum"]}, '{param["name"]}', {param["type"]}, {param["freqUnitsType"]},
                    {param["paramUnitsType"]}, {param["paramAmpFactorType"]}, {param["minFreq"]}, {param["maxFreq"]}, {param["alarmOnLocation"]}, {insert[1]},
                    {param["worldviewSystemId"]})
                """)

                cursor.execute(wp_stmt)
                wp_result = cursor.fetchall()[0]

                insert_wp.append(wp_result[0])

                wp_ids.append(insert_wp)
            
            # After all wave params have been inserted, flatten list of inserted param ids
            wp_ids = [id for list_ids in wp_ids for id in list_ids]
            
            alset_insert = ALSets[ALSets["APS_REF_ID"] == (ix+1)]

            alset_ids: list = []

            for _, alset in alset_insert.iterrows():
                insert_als = []

                als_stmt = (f"""
                Insert into ALSets (name, apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values('{alset["name"]}', 
                {insert[1]}, {alset["worldviewSystemId"]})""")

                cursor.execute(als_stmt)
                als_result = cursor.fetchall()[0]

                insert_als.append(als_result[0])
                alset_ids.append(insert_als)

                # Replace any NaN values with NULL
                AlarmLimits.fillna('NULL', axis=1, inplace=True)
                # Only insert the alarm limits that match this current apset
                al_insert = AlarmLimits[AlarmLimits["ALS_REF_ID"] == alset['ALS_REF_ID']]
                al_insert.reset_index(inplace=True)

                for ind, parameter in enumerate(wp_ids):

                    al_stmt = (f"""Insert into AlarmLimits (dwell, lowFaultActive, lowAlertActive, highFaultActive, highAlertActive, 
                    lowFault, highFault, lowAlert, highAlert, alsetId, waveParamId, autoLimits, worldviewSystemId) values(
                    {al_insert.loc[ind, "dwell"]}, {al_insert.loc[ind,"lowFaultActive"]}, {al_insert.loc[ind,"lowAlertActive"]}, 
                    {al_insert.loc[ind,"highFaultActive"]}, {al_insert.loc[ind,"highAlertActive"]}, {al_insert.loc[ind,"lowFault"]}, 
                    {al_insert.loc[ind,"highFault"]}, {al_insert.loc[ind,"lowAlert"]},{al_insert.loc[ind,"highAlert"]}, {als_result[0]}, {parameter},
                    {al_insert.loc[ind, "autoLimits"]}, {al_insert.loc[ind, "worldviewSystemId"]})""")

                    cursor.execute(al_stmt)

            alset_ids = [alsid for list_ids in alset_ids for alsid in list_ids]

            for alset_id in alset_ids:
                results.loc[len(results)] = [SensorType, APS, alset_id]

    except Exception as e:
        logging.error(f"Problem inserting apsets: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []

    connection.commit()
    cursor.close()
    connection.close()

    return results


def _insert_tachometers(engine, Tachometers):
    tach_ids: list = []

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        print(f"Inserting tachometers into the database...")

        for _, row in Tachometers.iterrows():
            tach_stmt = (f"""
                Insert into Tachometers (type, typicalRPM, name, pulsesPerRev, speedRatio, speedUnit, 
                triggerOnRisingEdge, inputChannelNum, spectralTach, worldviewSystemId) output inserted.[id] values(
                {row["type"]}, {row["typicalRPM"]}, '{row["name"]}', {row["pulsesPerRev"]}, {row["speedRatio"]},
                {row["speedUnit"]}, {row["triggerOnRisingEdge"]}, {row["inputChannelNum"]}, {row["spectralTach"]}, 
                {row["worldviewSystemId"]})""")
            
            cursor.execute(tach_stmt)
            result = cursor.fetchall()[0]

            tach_ids.append(result[0])

    except Exception as e:
        logging.error(f"Problem inserting tachometers: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []
    
    connection.commit()
    cursor.close()
    connection.close()

    print(f"Successfully inserted {len(tach_ids)} tachometers into {gu.wv_db}.")
    return tach_ids


def _insert_acceleration_apset(engine, acceleration):

    apset_ids: list = []

    results = pd.DataFrame(columns=["SensorType", "APSet_Id", "ALSet_Id"])

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        insert = []
        aps_stmt = (f"""
            Insert into APSets (name, sensorType, process, minFreq, maxFreq, resolution, average, frequencyUnit,  frequencyPlotUnit,
            waveformUnit, spectralUnit, spectralAmpFactor, autoscaleWaveform, autoscaleSpectral, orderEnabled, isDemod, logEnabled,
            waveformLogEnabled, worldviewSystemId) output 0, inserted.[id], inserted.[sensorType] values('VIBE DEFAULT', 0, 
            0, 0, 488, 1600, NULL, 1, 1, 0, 5, 1, 1, 1, 1, 0, 0, 0, 1)""")

        cursor.execute(aps_stmt)
        result = cursor.fetchall()[0]
        
        for i in range(len(result)):
            insert.append(result[i])

        apset_ids.append(insert)

        _, APS, SensorType = result

        wp_ids: list = []

        insert_wp = []

        wp_stmt = (f"""Insert into WaveParams (paramNum, name, type, freqUnitsType, paramUnitsType, paramAmpFactorType, minFreq, maxFreq, alarmOnLocation, 
            apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values(1, 'OVERALL', 0, 1,
            5, 1, 0, 0, 0, {insert[1]}, 1)""")

        cursor.execute(wp_stmt)
        wp_result = cursor.fetchall()[0]

        insert_wp.append(wp_result[0])

        wp_ids.append(insert_wp)
    
        # After all wave params have been inserted, flatten list of inserted param ids
        wp_ids = [id for list_ids in wp_ids for id in list_ids]

        alset_ids: list = []

        insert_als = []

        als_stmt = (f"""Insert into ALSets (name, apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values('DEFAULT', 
        {insert[1]}, 1)""")

        cursor.execute(als_stmt)
        als_result = cursor.fetchall()[0]

        insert_als.append(als_result[0])
        alset_ids.append(insert_als)

        al_stmt = (f"""Insert into AlarmLimits (dwell, lowFaultActive, lowAlertActive, highFaultActive, highAlertActive, 
        lowFault, highFault, lowAlert, highAlert, alsetId, waveParamId, autoLimits, worldviewSystemId) values(
        3, 0, 0, 1, 1, 0, 0.03, 0,0.02, {als_result[0]}, {wp_ids[0]}, 0, 1)""")

        cursor.execute(al_stmt)

        alset_ids = [alsid for list_ids in alset_ids for alsid in list_ids]

        for alset_id in alset_ids:
            results.loc[len(results)] = [SensorType, APS, alset_id]

    except Exception as e:
        logging.error(f"Problem inserting acceleration apsets: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []

    connection.commit()
    cursor.close()
    connection.close()

    acceleration = acceleration.append(results)

    return acceleration


def _insert_ultrasonic_apset(engine, ultrasonic):
    apset_ids: list = []

    results = pd.DataFrame(columns=["SensorType", "APSet_Id", "ALSet_Id"])

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        insert = []
        aps_stmt = (f"""
            Insert into APSets (name, sensorType, process, minFreq, maxFreq, resolution, average, frequencyUnit,  frequencyPlotUnit,
            waveformUnit, spectralUnit, spectralAmpFactor, autoscaleWaveform, autoscaleSpectral, orderEnabled, isDemod, logEnabled,
            waveformLogEnabled, worldviewSystemId) output 0, inserted.[id], inserted.[sensorType] values('ULTRA DEFAULT', 10, 
            0, 0, 488, 1600, NULL, 1, 1, 8, 7, 1, 1, 1, 1, 0, 0, 0, 1)""")

        cursor.execute(aps_stmt)
        result = cursor.fetchall()[0]
        
        for i in range(len(result)):
            insert.append(result[i])

        apset_ids.append(insert)

        _, APS, SensorType = result

        wp_ids: list = []

        insert_wp = []

        wp_stmt = (f"""Insert into WaveParams (paramNum, name, type, freqUnitsType, paramUnitsType, paramAmpFactorType, minFreq, maxFreq, alarmOnLocation, 
            apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values(1, 'OVERALL', 0, 1,
            7, 1, 0, 0, 0, {insert[1]}, 1)""")

        cursor.execute(wp_stmt)
        wp_result = cursor.fetchall()[0]

        insert_wp.append(wp_result[0])

        wp_ids.append(insert_wp)
    
        # After all wave params have been inserted, flatten list of inserted param ids
        wp_ids = [id for list_ids in wp_ids for id in list_ids]

        alset_ids: list = []

        insert_als = []

        als_stmt = (f"""Insert into ALSets (name, apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values('DEFAULT', 
        {insert[1]}, 1)""")

        cursor.execute(als_stmt)
        als_result = cursor.fetchall()[0]

        insert_als.append(als_result[0])
        alset_ids.append(insert_als)

        al_stmt = (f"""Insert into AlarmLimits (dwell, lowFaultActive, lowAlertActive, highFaultActive, highAlertActive, 
        lowFault, highFault, lowAlert, highAlert, alsetId, waveParamId, autoLimits, worldviewSystemId) values(
        3, 0, 0, 1, 1, 0, 106, 0,100, {als_result[0]}, {wp_ids[0]}, 0, 1)""")

        cursor.execute(al_stmt)

        alset_ids = [alsid for list_ids in alset_ids for alsid in list_ids]

        for alset_id in alset_ids:
            results.loc[len(results)] = [SensorType, APS, alset_id]

    except Exception as e:
        logging.error(f"Problem inserting ultrasonic apsets: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []

    connection.commit()
    cursor.close()
    connection.close()

    ultrasonic = ultrasonic.append(results)

    return ultrasonic


def _insert_temperature_apset(engine, temperature):
    apset_ids: list = []

    results = pd.DataFrame(columns=["SensorType", "APSet_Id", "ALSet_Id"])

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        insert = []
        aps_stmt = (f"""
            Insert into APSets (name, sensorType, process, minFreq, maxFreq, resolution, average, frequencyUnit,  frequencyPlotUnit,
            waveformUnit, spectralUnit, spectralAmpFactor, autoscaleWaveform, autoscaleSpectral, orderEnabled, isDemod, logEnabled,
            waveformLogEnabled, worldviewSystemId) output 0, inserted.[id], inserted.[sensorType] values('TEMP DEFAULT', 3, 
            1, NULL, NULL, NULL, NULL, NULL, NULL, 100, 100, 1, 1, 1, 1, 0, 0, 0, 1)""")

        cursor.execute(aps_stmt)
        result = cursor.fetchall()[0]
        
        for i in range(len(result)):
            insert.append(result[i])

        apset_ids.append(insert)

        _, APS, SensorType = result

        alset_ids: list = []

        insert_als = []

        als_stmt = (f"""Insert into ALSets (name, apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values('DEFAULT', 
        {insert[1]}, 1)""")

        cursor.execute(als_stmt)
        als_result = cursor.fetchall()[0]

        insert_als.append(als_result[0])
        alset_ids.append(insert_als)

        al_stmt = (f"""Insert into AlarmLimits (dwell, lowFaultActive, lowAlertActive, highFaultActive, highAlertActive, 
        lowFault, highFault, lowAlert, highAlert, alsetId, waveParamId, autoLimits, worldviewSystemId) values(
        3, 1, 1, 1, 1, 60, 120, 70, 110, {als_result[0]}, NULL, 0, 1)""")

        cursor.execute(al_stmt)

        alset_ids = [alsid for list_ids in alset_ids for alsid in list_ids]

        for alset_id in alset_ids:
            results.loc[len(results)] = [SensorType, APS, alset_id]

    except Exception as e:
        logging.error(f"Problem inserting temperature apsets: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []

    connection.commit()
    cursor.close()
    connection.close()

    temperature = temperature.append(results)

    return temperature


def _insert_battery_apset(engine, battery):
    apset_ids: list = []

    results = pd.DataFrame(columns=["SensorType", "APSet_Id", "ALSet_Id"])

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        insert = []
        aps_stmt = (f"""
            Insert into APSets (name, sensorType, process, minFreq, maxFreq, resolution, average, frequencyUnit,  frequencyPlotUnit,
            waveformUnit, spectralUnit, spectralAmpFactor, autoscaleWaveform, autoscaleSpectral, orderEnabled, isDemod, logEnabled,
            waveformLogEnabled, worldviewSystemId) output 0, inserted.[id], inserted.[sensorType] values('BATTERY DEFAULT', 11, 
            1, NULL, NULL, NULL, NULL, NULL, NULL, 140, 100, 1, 1, 1, 1, 0, 0, 0, 1)""")

        cursor.execute(aps_stmt)
        result = cursor.fetchall()[0]
        
        for i in range(len(result)):
            insert.append(result[i])

        apset_ids.append(insert)

        _, APS, SensorType = result

        alset_ids: list = []

        insert_als = []

        als_stmt = (f"""Insert into ALSets (name, apsetId, worldviewSystemId) output inserted.[id], inserted.[apsetId] values('DEFAULT', 
        {insert[1]}, 1)""")

        cursor.execute(als_stmt)
        als_result = cursor.fetchall()[0]

        insert_als.append(als_result[0])
        alset_ids.append(insert_als)

        al_stmt = (f"""Insert into AlarmLimits (dwell, lowFaultActive, lowAlertActive, highFaultActive, highAlertActive, 
        lowFault, highFault, lowAlert, highAlert, alsetId, waveParamId, autoLimits, worldviewSystemId) values(
        3, 1, 1, 0, 0, 3.2, 5.5, 3.3, 5, {als_result[0]}, NULL, 0, 1)""")

        cursor.execute(al_stmt)

        alset_ids = [alsid for list_ids in alset_ids for alsid in list_ids]

        for alset_id in alset_ids:
            results.loc[len(results)] = [SensorType, APS, alset_id]

    except Exception as e:
        logging.error(f"Problem inserting battery apsets: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []

    connection.commit()
    cursor.close()
    connection.close()

    battery = battery.append(results)

    return battery


def _check_apsets(engine, all_apsets: pd.DataFrame):

    acceleration = all_apsets[all_apsets["SensorType"]==0]
    ultrasonic = all_apsets[all_apsets["SensorType"]==10]
    temperature = all_apsets[all_apsets["SensorType"]==3]
    battery = all_apsets[all_apsets["SensorType"]==11]

    if len(acceleration) <= 0:
        acceleration = _insert_acceleration_apset(engine, acceleration)
    if len(ultrasonic) <= 0:
        ultrasonic = _insert_ultrasonic_apset(engine, ultrasonic)
    if len(temperature) <= 0:
        temperature = _insert_temperature_apset(engine, temperature)
    if len(battery) <= 0:
        battery = _insert_battery_apset(engine, battery)

    return acceleration, ultrasonic, temperature, battery


def create_apsets_assoc(engine):
    
    # Read-in apset setup file
    setup_file = pd.ExcelFile('APSetsDEFAULT.xlsx')

    # Build sampling dataframes
    APSets = pd.read_excel(setup_file, sheet_name='APSets')
    WaveParams = pd.read_excel(setup_file, sheet_name='WaveParams')
    ALSets = pd.read_excel(setup_file, sheet_name='ALSets')
    AlarmLimits = pd.read_excel(setup_file, sheet_name='AlarmLimits')
    Tachometers = pd.read_excel(setup_file, sheet_name='Tachometers')

    results = _insert_apsets(engine, APSets, WaveParams, ALSets, AlarmLimits)

    acceleration, ultrasonic, temperature, battery = _check_apsets(engine, results)

    tachometers = _insert_tachometers(engine, Tachometers)

    return acceleration, ultrasonic, temperature, battery, tachometers
