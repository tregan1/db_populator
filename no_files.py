import datetime
import pandas as pd
import numpy as np
import logging
from tqdm import tqdm
from array import array
import lzma
from typing import Optional
import codecs
from base64 import b64encode, b64decode
import random

import generator_utils as gu


def generate_timestamps(num_waves):
    timestamps = []
    init_dt = datetime.datetime.now() - datetime.timedelta(minutes=15)

    for i in range(num_waves):
        timestamps.append(init_dt - datetime.timedelta(minutes=i*240))

    return timestamps


def get_wave_info(engine, hwu_ids):
    if len(hwu_ids) == 1:
        hwu_ids = f"({hwu_ids[0]})"
    else:
        hwu_ids = tuple(hwu_ids)

    wave_stmt = f"""select 
  case
      when APSets.sensorType = 0 then 0
	  else 8
  end as "internalAmpUnit",
  0 as "internalTimeUnit",
  Channels.id as "channelId",
  -1 as "IDP",
  0 as "isDemod",
  case
    when APSets.[maxFreq] = 61 then 156
	when APSets.[maxFreq] = 122 then 312
	when APSets.[maxFreq] = 244 then 625
	when APSets.[maxFreq] = 488 then 1250
	when APSets.[maxFreq] = 977 then 2500
	when APSets.[maxFreq] = 1953 then 5000
	when APSets.[maxFreq] = 3906 then 10000
	when APSets.[maxFreq] = 7813 then 20000
	else NULL
  end as "sampleRate",
  case
    when APSets.[resolution] = 400 then 1024
	when APSets.[resolution] = 800 then 2048
	when APSets.[resolution] = 1600 then 4096
	when APSets.[resolution] = 3200 then 8192
	when APSets.[resolution] = 6400 then 16384
	when APSets.[resolution] = 12800 then 32768
	when APSets.[resolution] = 25600 then 65536
	when APSets.[resolution] = 51200 then 131072
	else NULL
  end as "sampleLength",
  HardwareUnits.serialNumber as "serialNumber",
  0 as "invalidFlag",
  1 as "worldviewSystemId",
  APSets.minFreq as "minFreq",
  APSets.resolution as "resolution",
  APSets.maxFreq as "maxFreq"
  from Channels 
  left join APSets on Channels.apsetId = APSets.id
  left join HardwareUnits on Channels.hardwareUnitId = HardwareUnits.id
  where Channels.type = 0 and HardwareUnits.id in {hwu_ids}"""

    wave_info = gu.sql_query(wave_stmt, engine)

    return wave_info


def compress(floatlist) -> Optional[bytes]:
    floatlist = floatlist.tolist()
    if floatlist is None:
        return None
        
    binary = array('d', floatlist).tobytes()
    return lzma.compress(binary)


def get_param_info(engine, hwu_ids):
    if len(hwu_ids) == 1:
        hwu_ids = f"({hwu_ids[0]})"
    else:
        hwu_ids = tuple(hwu_ids)

    params_stmt = f"""
SELECT 
       [Channels].[id] as "channelId"
      ,[Channels].[type] as "channelType"
      ,[Channels].[channelNum] as "channelNum"
      ,[Channels].[hardwareUnitId] as "hwunitId"
      ,[Channels].[apsetId] as "apsetId"
      ,[Channels].[alsetId] as "alsetId"
      ,[Channels].[assetId] as "assetId"
	  ,[WaveParams].[id] as "paramId"
  FROM [Channels]
  left join WaveParams on Channels.apsetId = WaveParams.apsetId
  left join HardwareUnits on Channels.hardwareUnitId = HardwareUnits.id
  where HardwareUnits.id in {hwu_ids}
    """

    param_info = gu.sql_query(params_stmt, engine)

    return param_info

def insert_ac_data(engine, hwu_ids, num_waves):

    wave_info = get_wave_info(engine, hwu_ids)

    param_info = get_param_info(engine, hwu_ids)

    full_set = pd.merge(wave_info[["channelId", "isDemod", "worldviewSystemId"]], param_info[["channelId", "channelType", "paramId"]], how='outer', left_on='channelId', right_on='channelId')

    with open("vibe_wave.txt", "r") as vibe_file:
        vibe_wave = np.genfromtxt(vibe_file, dtype=np.float, delimiter = ',')
    vibe_wave = compress(vibe_wave)
    vibe_wave = codecs.encode(vibe_wave,'hex').decode()
    vibe_wave = '0x' + vibe_wave

    with open("ultra_wave.txt", "r") as ultra_file:
        ultra_wave = np.genfromtxt(ultra_file, dtype=np.float, delimiter = ',')
    ultra_wave = compress(ultra_wave)
    ultra_wave = codecs.encode(ultra_wave,'hex').decode()
    ultra_wave = '0x' + ultra_wave

    with open("vibe_spectrum.txt", "r") as vibe_file:
        vibe_spectrum = np.genfromtxt(vibe_file, dtype=np.float, delimiter=',')
    vibe_spectrum = compress(vibe_spectrum)
    vibe_spectrum = codecs.encode(vibe_spectrum, 'hex').decode()
    vibe_spectrum = '0x' + vibe_spectrum

    with open("ultra_spectrum.txt", "r") as ultra_file:
        ultra_spectrum = np.genfromtxt(ultra_file, dtype=np.float, delimiter=',')
    ultra_spectrum = compress(ultra_spectrum)
    ultra_spectrum = codecs.encode(ultra_spectrum, 'hex').decode()
    ultra_spectrum = '0x' + ultra_spectrum

    wave_info["wavePoints"] = wave_info.apply( lambda row: vibe_wave if row['internalAmpUnit'] == 0 else ultra_wave, axis=1)

    wave_info["spectralPoints"] = wave_info.apply( lambda row: vibe_spectrum if row['internalAmpUnit'] == 0 else ultra_spectrum, axis=1)

    wave_info = wave_info.where(pd.notnull(wave_info), None)
    #wave_info.fillna('NULL', axis=1, inplace=True)

    timestamps = generate_timestamps(num_waves)

    wave_stmt = "Insert into WaveData (timestamp, internalAmpUnit, internalTimeUnit, wavePoints, channelId, IDP, isDemod, " +\
        "sampleRate, sampleLength, serialNumber, invalidFlag, worldviewSystemId) output inserted.id values ("

    spec_stmt = "Insert into SpectralData (timestamp, spectralPoints, channelId, isDemod, sampleRate, minFreq, maxFreq, resolution, worldviewSystemId) " +\
        "output inserted.id values ("

    trend_datum_stmt = "Insert into TrendData (timestamp, processed, alarm, channelId, waveDatumId, spectralDatumId, worldviewSystemId) output inserted.id " +\
        "values ("

    trend_vals_stmt = "Insert into TrendValues (avgValue, channelId, waveParamId, trendDatumId, isDemod, worldviewSystemId) values ("

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()
        
        #AC Data
        print(f"Inserting wave data into the database...")
        for _, row in tqdm(wave_info.iterrows(), total=wave_info.shape[0]):
            for record in timestamps:
                try:
                    wave_vals = f' \'{record.strftime("%Y-%m-%d %H:%M:%S")}\', {row["internalAmpUnit"]}, {row["internalTimeUnit"]},{row["wavePoints"]}, {row["channelId"]},' +\
                    f' {row["IDP"]}, {row["isDemod"]}, {row["sampleRate"]}, {row["sampleLength"]}, {row["serialNumber"]}, {row["invalidFlag"]}, {row["worldviewSystemId"]})'

                    cursor.execute(wave_stmt+wave_vals)
                    wave_result = cursor.fetchall()[0][0]

                    spec_vals = f' \'{record.strftime("%Y-%m-%d %H:%M:%S")}\', {row["spectralPoints"]}, {row["channelId"]},' +\
                    f' {row["isDemod"]}, {row["sampleRate"]}, {row["minFreq"]}, {row["maxFreq"]}, {row["resolution"]}, {row["worldviewSystemId"]})'

                    cursor.execute(spec_stmt+spec_vals)
                    spec_result = cursor.fetchall()[0][0]

                    trend_dat_vals = f' \'{record.strftime("%Y-%m-%d %H:%M:%S")}\', 0, 0, {row["channelId"]},' +\
                    f' {wave_result}, {spec_result}, {row["worldviewSystemId"]})'

                    cursor.execute(trend_datum_stmt+trend_dat_vals)
                    trend_result = cursor.fetchall()[0][0]

                    for _, value in full_set[full_set["channelId"] == row["channelId"]].iterrows():

                        trend_vals = f'{random.uniform(0.5, 1.2)}, {value["channelId"]}, {value["paramId"]}, {trend_result}, 0, {value["worldviewSystemId"]})'

                        cursor.execute(trend_vals_stmt+trend_vals)

                except Exception as e:
                    logging.error(f"Problem with inserting AC data: {e}")
        try:
            full_set.fillna('NULL', axis=1, inplace=True)
            dc_vals = full_set[full_set["channelType"] == 1]

            dc_trend_stmt = "Insert into TrendData (timestamp, processed, alarm, channelId, worldviewSystemId) output inserted.id " +\
            "values ("

            trend_vals_dc_stmt = "Insert into TrendValues (avgValue, channelId, trendDatumId, isDemod, worldviewSystemId) values ("

            print(f"Inserting DC trend data into database...")
            for _, dc_val in tqdm(dc_vals.iterrows(), total=dc_vals.shape[0]):
                for record in timestamps:

                    trend_dat_vals_dc = f' \'{record.strftime("%Y-%m-%d %H:%M:%S")}\', 0, 0, {dc_val["channelId"]}, {row["worldviewSystemId"]})'

                    cursor.execute(dc_trend_stmt+trend_dat_vals_dc)
                    trend_dc_result = cursor.fetchall()[0][0]

                    trend_vals_dc = f'{random.uniform(0.5, 1.2)}, {dc_val["channelId"]}, {trend_dc_result}, 0, 1)'

                    cursor.execute(trend_vals_dc_stmt+trend_vals_dc)
        except Exception as e:
            logging.error(f"Problem with inserting DC data: {e}")

    except Exception as e:
        logging.error(f"Problem inserting wave data: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []
    
    connection.commit()
    cursor.close()
    connection.close()

    return


if __name__ == "__main__":
        
    with open("vibe_wave.txt", "r") as vibe_file:
        vibe_wave = np.genfromtxt(vibe_file, dtype=np.float, delimiter=',')
    vibe_wave = compress(vibe_wave)
    vibe_wave = codecs.encode(vibe_wave,'hex').decode()

    print(type(vibe_wave))
    print(vibe_wave)
