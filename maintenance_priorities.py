import random
import lorem
import logging
import pandas as pd
from tqdm import tqdm

import generator_utils as gu

ACTIONS = [
    "Change Bearing",
    "Trim Balance",
    "Align Component",
    "Inspect Component",
    "Clean Component",
    "Repair Component",
    "Replace Component"
]

ISSUES = [
    "Bearing Fault",
    "BPFO Defect",
    "BPFI Defect",
    "BSF Defect",
    "FTF Defect",
    "Misalignment",
    "Structural Looseness",
    "Mechanical Looseness",
    "Unbalance",
    "Bent Shaft",
    "Structural Resonance",
    "Coupling Issue",
    "Gear Defect",
    "Motor Electrical Defect"
]

LEVELS =  [1,2,3,4]


def get_user_list(engine):
    users_stmt = "Select [id] from [Users]"

    users = gu.sql_query(users_stmt, engine)

    return users.values.tolist()


def get_channel_param_configs(engine, mp_channels):

    sql_stmt = f"""select top({mp_channels}) Ch.id as "ChannelId",
Wp.id as "ParamId",
Ch.type as "ChannelType"
from Channels Ch
left join WaveParams Wp on Ch.apsetId = Wp.apsetId
order by newid()"""

    ch_wp_results = gu.sql_query(sql_stmt, engine)

    ch_wp_results.sort_values(by=["ChannelId"], inplace=True, ascending=True)

    channels = ch_wp_results.ChannelId.unique()

    return ch_wp_results, channels


def generate_maintenance_priorities(engine, mp_channels):
    """
    Generates maintenance priorities for a subset of the channels created.
    """

    users = get_user_list(engine)

    ch_wp, channels = get_channel_param_configs(engine, mp_channels)

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        print(f"Inserting maintenance priorities into the database...")

        for ch in tqdm(channels):
            insert = []
            aps_stmt = (f"""
                Insert into PriorityLists (channelId, addUserId, priority, issue, action, description, worldviewSystemId) 
                output inserted.[id] values('{ch}',{random.choice(users[0])},{random.choice(LEVELS)}, '{random.choice(ISSUES)}', 
                '{random.choice(ACTIONS)}', '{lorem.sentence()}', '{1}')
                """)

            cursor.execute(aps_stmt)
            result = cursor.fetchall()[0]

            ListId = result

            # Replace any NaN values with NULL
            ch_wp.fillna('NULL', axis = 1, inplace=True)

            # Wave params to insert on this iteration match the intended apset
            wp_insert = ch_wp[ch_wp["ChannelId"] == ch]

            wp_ids: list = []

            for _, param in wp_insert.iterrows():

                if param["ParamId"] != 'NULL':
                    wp_stmt = (f"""
                    Insert into PriorityParameters (paramId, priorityListId, worldviewSystemId) values({param["ParamId"]}, {ListId[0]} ,'{1}')
                    """)

                    cursor.execute(wp_stmt)
                else:
                    break

    except Exception as e:
        logging.error(f"Problem inserting maintenance priorities: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []

    connection.commit()
    cursor.close()
    connection.close()

    return
