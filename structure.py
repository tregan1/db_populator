"""
Populates the "structure" of the database, as specified by the user.
"""

import logging
import pandas as pd
import math
from datetime import datetime
import random
from sqlalchemy.orm import sessionmaker
import sqlalchemy
from tqdm import tqdm

import generator_utils as gu
import analysis_utils as au


def check_base_ports(engine) -> list:
    """
    Get list of all base stations currently in the database.
    Future created base stations cannot use these port numbers.
    """

    port_query = f"Select [id], [port] from {gu.wv_db}.[dbo].[BaseStations]"

    ports = gu.sql_query(port_query, engine)

    port_nums = ports["port"].sort_values(ascending=True)
    
    return port_nums.tolist()


def _next_available_port(existing_ports: list) -> tuple:
    """
    Given a list of currently existing port numbers, finds the next 
    lowest available valid port number.
    """

    new_port = 702

    for val in existing_ports:
        if val == new_port:
            new_port += 2
        else:
            break
    
    existing_ports.append(new_port)

    return new_port, existing_ports


def create_base_stations(engine, schematic, hwu_ratio, default_base_ip):

    base_stations = pd.DataFrame(columns=['name', 'IPAddress', 'port', 'worldviewSystemId'])

    # Compute the number of base stations that need to be added to the db, based on the config-specified hwu_ratio
    num_base_stations = math.ceil(schematic["num_mists"] / hwu_ratio)

    existing_ports = check_base_ports(engine)

    new_port_ids = []

    for i in range(num_base_stations):
        base_name = f"BaseStation{i+1}"

        if len(existing_ports) > 0:
            port_num, existing_ports = _next_available_port(existing_ports)
            existing_ports.sort()
        else:
            port_num = 702
            existing_ports.append(port_num)

        base_stations.loc[i] = [base_name, default_base_ip, port_num, 1]
    
    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        logging.info(f"Inserting {num_base_stations} base stations into {gu.wv_db}")

        for _, row in base_stations.iterrows():
            insert = []
            stmt = (f"""Insert into {gu.wv_db}.[dbo].[BaseStations] ([name], [IPAddress], [port], [worldviewSystemId]) 
                    output inserted.[id] values
                    ('{row['name']}', '{row['IPAddress']}', {row['port']}, {row['worldviewSystemId']})""")

            cursor.execute(stmt)
            result = cursor.fetchall()[0]
            cursor.commit()

            for i in range(len(result)):
                insert.append(result[i])
            
            new_port_ids.append(insert[0])

    except Exception as e:
        logging.error(f"Error trying to execute sql statement. {e}")
    
    cursor.close()
    connection.close()

    return new_port_ids


def create_areas(engine, schematic):

    areas = pd.DataFrame(columns=['name', 'worldviewSystemId'])

    new_area_ids = []

    for i in range(schematic["num_areas"]):
        area_name = f"Area{i+1}"

        areas.loc[i] = [area_name, 1]
    
    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        logging.info(f"Inserting {schematic['num_areas']} areas into {gu.wv_db}...")

        print(f"Loading {schematic['num_areas']} areas into {gu.wv_db}...")
        for _, row in tqdm(areas.iterrows(), total=areas.shape[0]):
            insert = []
            stmt = (f"""Insert into {gu.wv_db}.[dbo].[Areas] ([name], [worldviewSystemId]) 
                    output inserted.[id] values
                    ('{row['name']}', {row['worldviewSystemId']})""")

            cursor.execute(stmt)
            result = cursor.fetchall()[0]

            for i in range(len(result)):
                insert.append(result[i])
            
            new_area_ids.append(insert[0])

            # Also insert custom area filters
            custom_filter_stmt = (f"""Insert into [CustomFilters]([userId], [name], [showGood], [showWarning], [showFault], [showAC], [showDC], [allUsers], [allAreas], [allAssets], [allChannels]) 
        output inserted.[name], inserted.[id] values""")

            val = (f" (1, '{row['name']}', 1, 1, 1, 1, 1, 1, 0, 1, 1)")

            cursor.execute(custom_filter_stmt + val)
            filter_result = cursor.fetchall()[0]

            custom_filter_id = filter_result[1]

            custom_area_stmt = (f"Insert into [CustomFilterAreas](customFilterId, areaId) values ({custom_filter_id}, {result[i]})")

            cursor.execute(custom_area_stmt)

    except Exception as e:
        logging.error(f"Error trying to execute sql statement. {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []
    
    connection.commit()
    cursor.close()
    connection.close()

    return new_area_ids


def create_assets(engine, schematic, new_area_ids):

    assets = pd.DataFrame(columns=['name', 'areaId', 'worldviewSystemId'])

    new_asset_ids = []

    for i in range(schematic["num_assets"]):
        asset_name = f"Asset{i+1}"

        assets.loc[i] = [asset_name, random.choice(new_area_ids), 1]
    
    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        logging.info(f"Inserting {schematic['num_assets']} assets into {gu.wv_db}...")

        print(f"Loading {schematic['num_assets']} assets into {gu.wv_db}")
        for _, row in tqdm(assets.iterrows(), total=assets.shape[0]):
            insert = []
            stmt = (f"""Insert into {gu.wv_db}.[dbo].[Assets] ([name], [areaId]) 
                    output inserted.[id] values
                    ('{row['name']}', {row['areaId']})""")

            cursor.execute(stmt)
            result = cursor.fetchall()[0]
            cursor.commit()

            for i in range(len(result)):
                insert.append(result[i])
            
            new_asset_ids.append(insert[0])

    except Exception as e:
        logging.error(f"Error trying to execute sql statement. {e}")
    
    cursor.close()
    connection.close()

    return new_asset_ids

   
def create_hardware_units(engine, schematic, new_port_ids):

    hwu = pd.DataFrame(columns=['name', 'type', 'serialNumber', 'disabled', 'baseStationId', 'decimationTime', 'worldviewSystemId'])

    new_hwu_ids = []
    MIST_FLOOR_SN: int = 3137339393
    MIST_TYPE: int = 2

    print(f"Building {schematic['num_mists']} mist hardware units for import to {gu.wv_db}...")

    for i in tqdm(range(schematic["num_mists"])):

        sn_int_val = MIST_FLOOR_SN + i

        hwu_name = str(hex(sn_int_val)).upper()[2:]

        hwu.loc[i] = [hwu_name, MIST_TYPE, sn_int_val, 0, random.choice(new_port_ids), ' ', 1]
    
    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        logging.info(f"Inserting {schematic['num_mists']} mists into {gu.wv_db}...")

        print(f"Loading {schematic['num_mists']} mists into {gu.wv_db}")
        for _, row in tqdm(hwu.iterrows(), total=hwu.shape[0]):
            insert = []
            stmt = (f"""Insert into {gu.wv_db}.[dbo].[HardwareUnits] ([name], [type], [serialNumber], [disabled],
                    [baseStationId], [decimationTime], [worldviewSystemId]) output inserted.[id] values
                    ('{row['name']}', {row['type']}, {row['serialNumber']}, {row['disabled']}, {row['baseStationId']}, 
                    '{row['decimationTime']}', {row['worldviewSystemId']})""")

            cursor.execute(stmt)
            result = cursor.fetchall()[0]
            cursor.commit()

            for i in range(len(result)):
                insert.append(result[i])
            
            new_hwu_ids.append(insert[0])

    except Exception as e:
        logging.error(f"Error trying to execute sql statement. {e}")
    
    cursor.close()
    connection.close()

    return new_hwu_ids


def create_alset(apset_id: int, engine, ch_name, cursor):
    alset_id = int()
    
    # Get list of wave param ids based on apset (can be zero for DC apsets)
    params_query = (f"""Select [id] from [waveParams] where [apsetId] = {apset_id}""")
    params = gu.sql_query(params_query, engine)
    param_list = params.values.tolist()
    param_ids = [id for param_val in param_list for id in param_val]

    # Create alset for a given channel and apset
    alset_stmt = (f"""Insert into ALSets (name, disabled, enableInvalid, apsetId, worldviewSystemId) output inserted.id values 
    ('APS{apset_id}_{ch_name}', 0, 1, {apset_id}, 1)""")

    cursor.execute(alset_stmt)
    alset_id = cursor.fetchall()[0][0]
    cursor.commit()
    
    # Create alarm limits for that alset
    if param_ids:
        for param in param_ids:
            param_limit_stmt = (f"""Insert into AlarmLimits (dwell, lowFaultActive, lowAlertActive, highFaultActive, highAlertActive, lowFault, highFault,
            lowAlert, highAlert, alsetId, waveParamId, worldviewSystemId) values (3, 0, 0, 1, 1, 0, 0.4, 0, 0.2, {alset_id}, {param}, 1)""")
            cursor.execute(param_limit_stmt)
            cursor.commit()
    else:
        param_limit_stmt = (f"""Insert into AlarmLimits (dwell, lowFaultActive, lowAlertActive, highFaultActive, highAlertActive, lowFault, highFault,
            lowAlert, highAlert, alsetId, waveParamId, worldviewSystemId) values (3, 0, 0, 1, 1, 0, 0.4, 0, 0.2, {alset_id}, NULL, 1)""")
        cursor.execute(param_limit_stmt)
        cursor.commit()

    return alset_id


def create_channels(engine, new_hwu_ids, acceleration, ultrasonic, temperature, battery, new_asset_ids, tachometers, alset_per_channel):
    idx = 1

    try:
        connection = engine.raw_connection()
        cursor = connection.cursor()

        channel_stmt = ("""Insert into Channels ([name], [type], [triggerAlarm], [channelNum], [sampleInterval], [sensitivity], 
        [offset], [importChannelId], [hardwareUnitId], [apsetId], [alsetId], [tachometerId], [assetId], [storeSpectrumInterval], [worldviewSystemId])""")

        print("Inserting channels into database...")
        for unit in tqdm(new_hwu_ids):

            choice_asset = random.choice(new_asset_ids)

            accel_ind = random.choice(range(len(acceleration)))
            accel_ap = acceleration.iloc[accel_ind]["APSet_Id"]
            
            ultra_ind = random.choice(range(len(ultrasonic)))
            ultra_ap = ultrasonic.iloc[ultra_ind]["APSet_Id"]

            temp_ind = random.choice(range(len(temperature)))
            temp_ap = temperature.iloc[temp_ind]["APSet_Id"]
            
            batt_ind = random.choice(range(len(battery)))
            batt_ap = battery.iloc[batt_ind]["APSet_Id"]

            if alset_per_channel:
                accel_al = create_alset(accel_ap, engine, idx, cursor)
                ultra_al = create_alset(ultra_ap, engine, idx, cursor)
                temp_al = create_alset(temp_ap, engine, idx, cursor)
                batt_al = create_alset(batt_ap, engine, idx, cursor)
            else:
                accel_al = acceleration.iloc[accel_ind]["ALSet_Id"]
                ultra_al = ultrasonic.iloc[ultra_ind]["ALSet_Id"]
                temp_al = temperature.iloc[temp_ind]["ALSet_Id"]
                batt_al = battery.iloc[batt_ind]["ALSet_Id"]

            tach_choice = random.choice(tachometers)

            ultra_stmt = (f""" values ('{str(idx)}_ULTRA', 0, 1, 0, 2400, 50, 1, null, {unit}, {ultra_ap}, {ultra_al}, {tach_choice}, {choice_asset}, 1, 1)""")
            vibe_stmt = (f""" values ('{str(idx)}_VIBE', 0, 1, 1, 2400, 50, 1, null, {unit}, {accel_ap}, {accel_al}, {tach_choice}, {choice_asset}, 1, 1)""")
            battery_stmt = (f""" values ('{str(idx)}_BATTERY', 1, 1, 1, 2400, 1000, 0, null, {unit}, {batt_ap}, {batt_al}, null, {choice_asset}, 1, 1)""")
            temp_stmt = (f""" values ('{str(idx)}_TEMP', 1, 1, 0, 2400, 1000, 0, null, {unit}, {temp_ap}, {temp_al}, null, {choice_asset}, 1, 1)""")
            
            cursor.execute(channel_stmt + ultra_stmt)
            cursor.execute(channel_stmt + vibe_stmt)
            cursor.execute(channel_stmt + battery_stmt)
            cursor.execute(channel_stmt + temp_stmt)

            idx += 1

    except Exception as e:
        logging.error(f"Problem inserting channels: {e}")
        connection.rollback()
        cursor.close()
        connection.close()
        return []

    connection.commit()
    cursor.close()
    connection.close()

    return


def create_structure(engine, schematic, hwu_ratio, default_base_ip, alset_per_channel):
    """
    Function to facilitate the creation of the components of the database.
    """

    new_port_ids = create_base_stations(engine, schematic, hwu_ratio, default_base_ip)

    new_area_ids = create_areas(engine, schematic)

    new_asset_ids = create_assets(engine, schematic, new_area_ids)

    acceleration, ultrasonic, temperature, battery, tachometers = au.create_apsets_assoc(engine)

    new_hwu_ids = create_hardware_units(engine, schematic, new_port_ids)

    create_channels(engine, new_hwu_ids, acceleration, ultrasonic, temperature, battery, new_asset_ids, tachometers, alset_per_channel)

    return new_hwu_ids
