import pandas as pd
import logging
import numpy as np


def initialize():
    global wv_db
    wv_db = ''


def sql_query(stmt, engine):
    """
    Executes a sql query within a connection that is rolled back in the event of an exception.
    """
    try:
        with engine.begin() as connection:
            results = pd.read_sql_query(stmt, connection)
            # read_sql_query only checks the first three values returned per column to determine column type
            # this can cause issues if the first three values are NULL, as it will default the column to Object.
            # Find and replace all instances with np.nan
            results_objects = list(results.select_dtypes(include=["object"]).columns.values)
            results[results_objects] = results[results_objects].replace([None], np.nan)
    except Exception as e:
        logging.error(f"Problem executing sql statement. {e}")
        results = []

    return results
